export const API = {
    baseUri: 'http://pte.novasoftware.com.au:3090/',
    initPosts: 'api/initPosts',
    increaseNumLike:'api1/increaseLike/',
    addComment: 'api/addComment',
    getComment:'api1/getCommentsByPostId/',
    deleteComment:'api1/deleteComment/',
    newPost: 'newPost',
    postMain:'postmain ',
    addFavoritePost:'api/addFavorite',
    getFavorite:'api1/myFavorites/',

    // POST a single image and get returned image name
    uploadImg: 'uploadImg',
    deletePost: 'api1/deletePost/',
    inittieba: 'api/initTieba',
    newTieba: 'api/newTieba',
    getAnswers: 'api1/getAnswersByTiebaId/',
    addAnswer: 'api/addAnswer',
    deleteAnswer: 'api1/deleteAnswer/',
    deleteTieba: 'api1/deleteTieba/',

}
