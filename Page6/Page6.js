import React, {Component} from 'react';
import { Text,View, Button } from 'react-native';
import Post from '../Post/Post';
import {StackNavigator} from 'react-navigation';
import ViewLife from './ViewLife';
import ViewPost from '../Post/ViewPost';

class Page6 extends Component {
  
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    return{
      headerRight: (
        <Button title="Post" color="#fff" onPress= {()=> {navigation.navigate('Post')}} />
 
      ),
    };
  }

  render () {
    return (
     <ViewLife {...this.props} />
    );
  }
}
const Stack=StackNavigator(
  {
    Home:{
      screen:Page6,
    },
    Post: {
      screen: Post,
    }, 
    viewPost: {
      screen: ViewPost,
    }
  },

  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }

);
export default Stack;

